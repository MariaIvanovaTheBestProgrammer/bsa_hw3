﻿using System;
    
namespace BSA_HW3.Data.Models
{
    public enum TaskState
    {
        ToDo,
        InProgress,
        Done,
        Canceled
    }
    public class Task
    {
        public int TaskId { get; set; }
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        public int PerformerId { get; set; }
        public User Permormer { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState TaskState { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

    }
}
