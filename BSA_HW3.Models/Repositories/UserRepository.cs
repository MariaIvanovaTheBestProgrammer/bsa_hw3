﻿using AutoMapper;
using BSA_HW3.Data.Interfaces;
using BSA_HW3.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BSA_HW3.Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        private static List<User> _users = new List<User>();

        public void CreateUser(User user)
        {
            user.Id = _users.Count + 1;
            _users.Add(user);
        }

        public void DeleteUser(int userId)
        {
            _users.Remove(_users.Where(e => e.Id == userId).FirstOrDefault());
        }

        public User GetUserById(int userId)
        {
            return _users.Where(e => e.Id == userId).FirstOrDefault();
        }

        public IEnumerable<User> GetUsers()
        {
            return _users;
        }

        public void UpdateUser(User user)
        {
            var tmp = _users.FirstOrDefault(e => e.Id == user.Id);
            if (tmp != null)
            {
                tmp.FirstName = user.FirstName;
                tmp.LastName = user.LastName;
                tmp.TeamId = user.TeamId;
                tmp.Team = user.Team;
                tmp.RegisteredAt = user.RegisteredAt;
                tmp.Email = user.Email;
                tmp.BirthDay = user.BirthDay;
            }
        }
    }
}
