﻿using BSA_HW3.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;
using BSA_HW3.Data.Models;

namespace BSA_HW3.Data.Repositories
{
    public class ProjectRepository : IProjectRepository
    {
        private static List<Project> _projects = new List<Project>();

        public void CreateProject(Project project)
        {
            project.Id = _projects.Count + 1;
            _projects.Add(project);
        }

        public void DeleteProject(int projectId)
        {
            _projects.Remove(_projects.Where(p => p.Id == projectId).FirstOrDefault());
        }


        public Project GetProjectById(int projectId)
        {
            return _projects.Where(e => e.Id == projectId).FirstOrDefault();
        }

        public IEnumerable<Project> GetProjects()
        {
            return _projects;
        }

        public void UpdateProject(Project project)
        {
            var tmp = _projects.FirstOrDefault(e => e.Id == project.Id);
            if (tmp != null)
            {
                tmp.Name = project.Name;
                tmp.TeamId = project.TeamId;
                tmp.Team = project.Team;
                tmp.AuthorId = project.AuthorId;
                tmp.User = project.User;
                tmp.CreatedAt = project.CreatedAt;
                tmp.Deadline = project.Deadline;
                tmp.Descriprion = project.Descriprion;
            }
        }

    }

}
