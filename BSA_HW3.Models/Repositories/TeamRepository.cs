﻿using AutoMapper;
using BSA_HW3.Data.Interfaces;
using BSA_HW3.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace BSA_HW3.Data.Repositories
{
    public class TeamRepository : ITeamRepository
    {
        private static List<Team> _teams = new List<Team>();

        public void CreateTeam(Team team)
        {
            team.Id = _teams.Count + 1;
            _teams.Add(team);
        }

        public void DeleteTeam(int teamId)
        {
            _teams.Remove(_teams.Where(e => e.Id == teamId).FirstOrDefault());
        }


        public Team GetTeamById(int teamId)
        {
            return _teams.Where(e => e.Id == teamId).FirstOrDefault();
        }

        public IEnumerable<Team> GetTeams()
        {
            return _teams;
        }

        public void UpdateTeam(Team team)
        {
            var tmp = _teams.FirstOrDefault(e => e.Id == team.Id);
            if (tmp != null)
            {
                tmp.Name = team.Name;
                tmp.CreatedAt = team.CreatedAt;
            }
        }

    }
}
