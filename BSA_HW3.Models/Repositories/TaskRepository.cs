﻿using AutoMapper;
using BSA_HW3.Data.Interfaces;
using System.Collections.Generic;
using System.Linq;
using BSA_HW3.Data.Models;

namespace BSA_HW3.Data.Repositories
{
    public class TaskRepository : ITaskRepository
    {
        private static List<Task> _tasks = new List<Task>();

        public void CreateTask(Task task)
        {
            task.TaskId = _tasks.Count + 1;
            _tasks.Add(task);
        }

        public void DeleteTask(int taskId)
        {
            _tasks.Remove(_tasks.Where(p => p.TaskId == taskId).FirstOrDefault());
        }

        public Task GetTaskById(int taskId)
        {
            return _tasks.Where(e => e.TaskId == taskId).FirstOrDefault();
        }

        public IEnumerable<Task> GetTasks()
        {
            return _tasks;
        }

        public void UpdateTask(Task task)
        {
            var tmp = _tasks.FirstOrDefault(e => e.TaskId == task.TaskId);
            if (tmp != null)
            {
                tmp.Name = task.Name;
                tmp.PerformerId = task.PerformerId;
                tmp.Permormer = task.Permormer;
                tmp.ProjectId = task.ProjectId;
                tmp.Project = task.Project;
                tmp.TaskState = task.TaskState;
                tmp.CreatedAt = task.CreatedAt;
                tmp.FinishedAt = task.FinishedAt;
                tmp.Description = task.Description;
            }
        }

        public IEnumerable<Task> GetUserTasks(int userId)
        {
            return _tasks.Where(t => t.PerformerId == userId && t.Name.Length < 45);
        }

        public IEnumerable<TaskInfo> GetFinishedTaskIdName(int userId)
        {
            return _tasks.Where(t => t.PerformerId == userId)
                        .Where(t => t.TaskState == TaskState.Done && t.FinishedAt?.Year == 2021)
                        .Select(t => new TaskInfo { Id = t.TaskId, Name = t.Name });
        }
    }
}
