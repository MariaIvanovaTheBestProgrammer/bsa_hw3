﻿using BSA_HW3.Data.Models;
using System.Collections.Generic;


namespace BSA_HW3.Data.Interfaces
{
    public interface IProjectRepository
    {
        IEnumerable<Project> GetProjects();
        Project GetProjectById(int projectId);
        void CreateProject(Project project);
        void DeleteProject(int projectId);
        void UpdateProject(Project project);
    }
}
