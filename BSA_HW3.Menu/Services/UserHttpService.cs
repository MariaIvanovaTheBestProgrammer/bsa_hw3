﻿using BSA_HW3.Common;
using BSA_HW3.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace BSA_HW3.Menu.Services
{
    public class UserHttpService
    {
        static readonly HttpClient client = new HttpClient();

        public async Task<IEnumerable<UserDTO>> GetUsers()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + "user");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<UserDTO>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }
        public async Task<UserDTO> GetUserById(int userId)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + $"user/{userId}");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<UserDTO>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }
        public async Task<IEnumerable<User>> GetUsersSortedByName()
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + "user/SortedTasks");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<User>>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }
        public async Task<UserProjectInfo> GetUserProjectInfo(int userId)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(Settings.basePath + $"user/UserProjectInfo/{userId}");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<UserProjectInfo>(responseBody);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return null;
            }
        }
        public async Task CreateUser(UserDTO userDTO)
        {
            try
            {
                var stringContent = new StringContent(JsonConvert.SerializeObject(userDTO), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PostAsync(Settings.basePath + "user", stringContent);
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }
        public async Task DeleteUser(int userId)
        {
            try
            {
                HttpResponseMessage response = await client.DeleteAsync(Settings.basePath + $"user/{userId}");
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }
        public async Task UpdateUser(UserDTO userDTO)
        {
            try
            {
                var stringContent = new StringContent(JsonConvert.SerializeObject(userDTO), Encoding.UTF8, "application/json");
                HttpResponseMessage response = await client.PutAsync(Settings.basePath + "user", stringContent);
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
            }
        }
    }
}
