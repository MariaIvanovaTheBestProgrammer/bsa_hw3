﻿
namespace BSA_HW3.Common
{
    public class ProjectInfo
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestDescriptionTask { get; set; }
        public TaskDTO ShortestNameTask { get; set; }
        public int ProjectTeamCount { get; set; }
    }
}
